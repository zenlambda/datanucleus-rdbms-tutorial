package com.example.rdbms_tutorial;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import com.example.rdbms_tutorial.model.Inventory;
import com.example.rdbms_tutorial.model.Product;

public class InsertApp {
	
	public static void main(String[] args) {
		PersistenceManagerFactory pmf = JDOHelper.getPersistenceManagerFactory("Tutorial");
		PersistenceManager pm = pmf.getPersistenceManager();
		
		Transaction tx=pm.currentTransaction();
		try
		{
		    tx.begin();
		    Inventory inv = new Inventory("My Inventory");
		    Product product = new Product("Sony Discman", "A standard discman from Sony", 49.99);
		    inv.getProducts().add(product);
		    pm.makePersistent(inv);
		    tx.commit();
		    System.out.println("INSERTED: "+inv);
		}
		finally
		{
		    if (tx.isActive())
		    {
		    	System.out.println("rollingback");
		        tx.rollback();
		    }
		    pm.close();
		}
	}

}
