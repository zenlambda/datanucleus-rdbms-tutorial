package com.example.rdbms_tutorial.model;

public class Book extends Product {
    String author=null;
    String isbn=null;
    String publisher=null;

    public Book(String name, String desc, double price, String author, 
                String isbn, String publisher)
    {
        super(name,desc,price);
        this.author = author;
        this.isbn = isbn;
        this.publisher = publisher;
    }

	@Override
	public String toString() {
		return "Book [author=" + author + ", isbn=" + isbn + ", publisher="
				+ publisher + "]";
	}
    
    
}
