package com.example.rdbms_tutorial.model;

import java.util.HashSet;
import java.util.Set;

public class Inventory {
    String name = null;
    Set<Product> products = new HashSet<>();

    public Inventory(String name)
    {
        this.name = name;
    }

    public Set<Product> getProducts() {return products;}

	@Override
	public String toString() {
		return "Inventory [name=" + name + ", products=" + products + "]";
	}
    
    
}
