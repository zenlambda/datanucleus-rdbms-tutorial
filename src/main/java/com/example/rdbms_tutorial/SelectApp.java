package com.example.rdbms_tutorial;

import java.util.Iterator;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.example.rdbms_tutorial.model.Product;

public class SelectApp {
	public static void main(String[] args) {
		PersistenceManagerFactory pmf = JDOHelper.getPersistenceManagerFactory("Tutorial");
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
		    tx.begin();

		    Query q = pm.newQuery("SELECT FROM " + Product.class.getName() + 
		                          " WHERE price < 150.00 ORDER BY price ASC");
			List<Product> products = (List<Product>) q.execute();
		    Iterator<Product> iter = products.iterator();
		    while (iter.hasNext())
		    {
		        Product p = iter.next();
		        
		        System.out.println("FOUND: "+p);

		    }

		    tx.commit();
		}
		finally
		{
		    if (tx.isActive())
		    {
		        tx.rollback();
		    }

		    pm.close();
		}
	}
}
