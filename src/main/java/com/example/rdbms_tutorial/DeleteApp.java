package com.example.rdbms_tutorial;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.example.rdbms_tutorial.model.Inventory;

public class DeleteApp {
	
	public static void main(String[] args) {
		PersistenceManagerFactory pmf = JDOHelper.getPersistenceManagerFactory("Tutorial");
		PersistenceManager pm = pmf.getPersistenceManager();
		
		Transaction tx = pm.currentTransaction();
		try
		{
		    tx.begin();

		    Query inventories = pm.newQuery(Inventory.class);
		    inventories.deletePersistentAll();
		    tx.commit();
		    System.out.println("DELETED: "+inventories);
		}
		finally
		{
		    if (tx.isActive())
		    {
		        tx.rollback();
		    }

		    pm.close();
		}
		
	}

}
